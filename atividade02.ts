type cientista = {
    nome: string,
    dataNascimento: Date,
    nacionalidade: string,
    bio: string,
    areasAtuacao: string[]
}

const cientistas: cientista[] = []

const dennisRitchie: cientista = {
    nome: 'Dennis Ritchie',
    dataNascimento: new Date(1941, 8, 9),
    nacionalidade: 'Norte Americano',
    bio: 'Was an American computer scientist.[1] He created the C programming language and, with long-time colleague Ken Thompson, the Unix operating system and B programming language',
    areasAtuacao: ['linguagens de programação', 'sistemas operacionais']
}

const graceHopper: cientista = {
    nome: 'Grace Murray Hopper',
    dataNascimento: new Date(1906, 11, 9),
    nacionalidade: 'Norte Americana',
    bio: 'Foi almirante e analista de sistemas da Marinha dos Estados Unidos nas décadas de 1940 e 1950, criadora da linguagem de programação de alto nível Flow-Matic',
    areasAtuacao: ['Analista de Sistemas', 'Criadora da linguagem Flow-Matic - base do COBOL -']
}

const alanTuring: cientista = {
    nome: 'Alan Mathison Turing',
    dataNascimento: new Date(1912, 5, 23),
    nacionalidade: 'Inglês',
    bio: 'Turing foi altamente influente no desenvolvimento da ciência da computação teórica, proporcionando uma formalização dos conceitos de algoritmo e computação com a máquina de Turing',
    areasAtuacao: ['Matemática', 'Ciência da Computação']
}

const johnVonNeumann: cientista = {
    nome: 'John von Neumann',
    dataNascimento: new Date(1903, 11, 28),
    nacionalidade: 'Húngaro',
    bio: 'Foi um matemático húngaro de origem judaica, naturalizado estadunidense.    Contribuiu na teoria dos conjuntos, análise funcional, teoria ergódica, mecânica quântica, ciência da computação, economia, teoria dos jogos, análise numérica, hidrodinâmica das explosões, estatística e muitas outras áreas da matemática',
    areasAtuacao: ['Fisica Nuclear', 'Projeto Manhattan']
}

cientistas.push(dennisRitchie, graceHopper, alanTuring, johnVonNeumann)







console.log('cientistas:', cientistas)