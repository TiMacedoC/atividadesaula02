let valorMaior: number = Math.floor(Math.random() * 11);
let valorMenor: number = Math.floor(Math.random() * 11);

if (valorMaior < valorMenor) {
    const aux = valorMenor
    valorMenor = valorMaior
    valorMaior = aux
}

console.log(`Valor Maior: ${valorMaior} \nValor Menor: ${valorMenor}`)

