const nome: string = 'Tiago';
const idade: number = 28;

const saudacao = 'Olá, meu nome é #nome# e eu tenho #idade# anos. \n#nome# como vai você?';

let novaSaudacao: string = saudacao.replace('#nome#', nome)
novaSaudacao = novaSaudacao.replace('#idade#', `${idade}`)

console.log(novaSaudacao);

